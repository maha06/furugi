from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from ..models import Dress, Dress_Pictures, Personal_Details, Favorites, Dress_Rating, Thread, Chat, User_Thread,Report, Ad_Thread
from django.http import Http404
from django.contrib.auth.decorators import login_required


@login_required
def user_management(request):
    if request.user.is_superuser:
        users = User.objects.prefetch_related('personal_details_set').all()
        data = {'users': users}
        return render(request, 'furugi/admin/user_management.html', data)
    else:
        raise Http404() 

@login_required
def deactivate_user(request, id):  
    if request.user.is_superuser:    
      try:
          user = User.objects.get(id = id)
          user.is_active = False
          user.save()
      except User.DoesNotExist:
          raise Http404()    
      return redirect('/admin/user-management')
    else:
      raise Http404()

@login_required
def activate_user(request, id):  
    if request.user.is_superuser:    
      try:
          user = User.objects.get(id = id)
          user.is_active = True
          user.save()
      except User.DoesNotExist:
          raise Http404()    
      return redirect('/admin/user-management')
    else:
      raise Http404()

@login_required
def user_reporting(request):
    if request.user.is_superuser:
        report = Report.objects.all()
        data = {'reports': report}
        return render(request, 'furugi/admin/user_reporting.html', data)
    else:
        raise Http404()

@login_required
def admin_dashboard(request):
    if request.user.is_superuser:
        return render(request, 'furugi/admin/dashboard.html')
    else:
        raise Http404() 