from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from ..models import Dress, Dress_Pictures, Trending, Personal_Details, Favorites, Dress_Rating, Thread, Chat, User_Thread, Ad_Thread, Blog
from datetime import datetime, date
from django.templatetags.static import static
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from taggit.models import Tag
from django.core.mail import send_mail
from django.db.models import Avg
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from random import randrange

def index(request):
    try:
        latest = Dress.objects.all().order_by('-posted_date')[:40]
    except Dress.DoesNotExist:
        raise Http404()
    try:
        trending = Trending.objects.all()
        start = randrange(15,25)        
        rated = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-posted_date')[start:][:11]
        #rated = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-views')[:11]
    except Dress.DoesNotExist:
        raise Http404()
    try:
        start = randrange(5,45)        
        popular = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-views')[start:][:9]
    except Dress.DoesNotExist:
        raise Http404()
    try:
        blog = Blog.objects.all().order_by('-date')[:3]
    except Blog.DoesNotExist:
        raise Http404()
    
    data = {'trending': trending, 'rated': rated , "popular": popular , "latest": latest, "blogs": blog }
    return render(request, 'furugi/index.html', data) 

@login_required
def get_notifications(request):
    user_thread = User_Thread.objects.filter(sender = request.user) 
    chat = 0   
    for user in user_thread:
        data = Chat.objects.filter(thread = user.thread, user= user.reciever, read = 0).count()
        chat += data    
    return JsonResponse({ 'data' :chat})

def blog_view(request, slug):
    blogs = Blog.objects.filter(slug = slug)
    data = {"blog": blogs[0]}
    return render(request, 'furugi/blogs/view.html', data)

def blogs(request):
    blogs = Blog.objects.all().order_by('-date')
    page = request.GET.get('page', 1)
    paginator = Paginator(blogs, 16)
    try:
        blogs = paginator.page(page)
    except PageNotAnInteger:
        blogs = paginator.page(1)
    except EmptyPage:
        bolgs = paginator.page(paginator.num_pages)
    data = {"blogs": blogs}
    return render(request, 'furugi/blogs/listing.html', data)

def contact(request):
    if request.POST:
        fname = request.POST.get('fname')
        lname = request.POST.get('lname')
        email = request.POST.get('email')
  
        subject = request.POST.get('subject')
        msg = request.POST.get('message')
        html = 'Name = '+fname+' '+lname +' Message = '+msg+'';
        html_message = render_to_string('furugi/emails/contact.html', {'fname': fname, 'lname' : lname, 'msg' : msg, 'email' : email}
        plain_message = strip_tags(html_message)
        send_mail(subject, plain_message, email, ['contact@furugi.com.pk'], html_message = html_message)
        messages.add_message(request, messages.SUCCESS, 'Email has been sent.')  
        return render(request, 'furugi/contact.html')      
    else:
        return render(request, 'furugi/contact.html')

def terms_and_conditions(request):
    return render(request, 'furugi/terms&conditions.html')

def privacy_policy(request):
    return render(request, 'furugi/privacy_policy.html')

def about(request):
    return render(request, 'furugi/about.html')

def error_404(request, exception):
    return render(request, 'furugi/404.html')

def error_500(request):
    return render(request, 'furugi/500.html')
