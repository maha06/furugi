from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from ..models import Dress, Dress_Pictures, Personal_Details, Favorites, Dress_Rating, Thread, Chat, User_Thread,Report, Ad_Thread, Block
from datetime import datetime, date
from django.templatetags.static import static
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from taggit.models import Tag
from django.core.mail import send_mail
from django.db.models import Avg
from django.http import Http404
from django.contrib.postgres.search import TrigramSimilarity
from django.db.models.functions import Greatest
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from twilio.rest import Client

@login_required
def chatbox(request):       
    user_thread = User_Thread.objects.filter(sender = request.user).order_by('-thread') 
    thread = list(user_thread)
    chat = []    
    for user in user_thread:
        data = Chat.objects.filter(thread = user.thread, user= user.reciever, read = 0).count()
        chat.append(data)
    data = {'data' : zip(chat,thread)}
    return render(request, 'furugi/chatbox.html', data)

@login_required
def create_thread(request, u_id, d_id):
    user = request.user
    dress = Dress.objects.get(id = d_id)
    reciever = User.objects.get(id = u_id) 
    if user == reciever:
        messages.add_message(request, messages.ERROR, 'Your cannot message yourself on your own Ads.')    
        return redirect('/user-ads')
    block = Block.objects.filter(user = user, reporter = reciever).count()
    if block > 0:
        messages.add_message(request, messages.ERROR, 'Respective advertiser has blocked you. So you cannot chat with them anymore.')    
        return redirect('/ads')
    Thread.objects.create()
    thread = Thread.objects.latest('id')    
    email = str(reciever.email)
    
    try:    
        User_Thread.objects.create(thread = thread, sender = user, reciever = reciever )
    except User_Thread.DoesNotExist:
        raise Http500()
    try:    
        User_Thread.objects.create(thread = thread, sender = User.objects.get(id = u_id), reciever = user )
    except User_Thread.DoesNotExist:
        raise Http500()
    try:    
        Chat.objects.create(thread = thread, message = request.POST.get('msg'), user = request.user )
    except Chat.DoesNotExist:
        raise Http500()
    try:    
        Ad_Thread.objects.create(thread = thread, dress = dress)
    except Ad_Thread.DoesNotExist:
        raise Http500()
    chat_notification(request, email)
    try:
        profile = Personal_Details.objects.get(user = reciever)
        if not profile:
            contact = ''
        else:
            contact = str(profile.contact)
            #sendMsg(request, contact)
    except Exception as e:
        pass
    return redirect('/chatbox')

@login_required
def delete_thread(request, id):
    try:
        Thread.objects.get(id = id).delete()
    except Exception as e:
        pass
    return redirect('/chatbox')

@login_required
def get_messages(request):
    id = request.GET.get('id', None)
    thread = Thread.objects.get(id = id)
    ad = Ad_Thread.objects.filter(thread = thread)
    dress = Dress.objects.filter(id = ad[0].dress.id).values()
    data = Chat.objects.filter(thread = thread).order_by('timestamp') 
    chat = data.values()
    data.exclude(user = request.user).update(read = 1)    
    data = {'chat': list(chat), 'ad': list(dress)}
    return JsonResponse(data)

@login_required
def post_message(request):
    id = request.GET.get('id', None)
    msg = request.GET.get('msg', None)
    try:
        c = Chat.objects.create(thread =  Thread.objects.get(id=id), message = msg, user = request.user )
    except Chat.DoesNotExist:
        raise Http500()
    data = {'message': msg, 'thread_id': id, 'timestamp': c.timestamp}
    return JsonResponse(data)

@login_required
def thread_advertisment(request, id):
    thread = Thread.objects.get(id = id)
    ad = Ad_Thread.objects.get(thread = thread)
    id = ad.dress.id
    return redirect('/ad-view/'+ id)

@login_required
def chat_notification(request, email):
    subject = "New Message Notification"
    html_message = render_to_string('partials/email.html', {'context': 'values'})
    plain_message = strip_tags(html_message)
    send_mail(subject, plain_message, 'CHAT NOTIFICATION <contact@furugi.com.pk>',[email], html_message= html_message)
 
@login_required
def sendMsg( request, number):
    account_sid = "AC733c99c53e17dc55c875fec65b77888c"
    auth_token  = "e28847557e7008dab4a2f8b7702f8138"
    client = Client(account_sid, auth_token)

    if number.startswith('0'):
        x = number
        remove = x[1:]
        number = '+92'+remove
    try:    
        message = client.messages.create(
        body="Dear costumer,\n You have recieved new message thread at FURUGI. Visit 'Chatbox' section on website now and respond well to your costumers.\n Regards, FURUGI Team",
        to= number,
        from_="+16105467688")
    except Exception as e:
       pass