from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
from ..models import Dress, Dress_Pictures, Personal_Details, Favorites, Block, Dress_Rating, Thread, Chat, Cart, User_Thread,Report, Ad_Thread, Trending
from datetime import datetime, date
from django.templatetags.static import static
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from taggit.models import Tag
from django.db.models import Avg
from django.http import Http404
from django.contrib.auth.decorators import login_required
from random import randrange


def register(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        username = request.POST.get('username')
        password = request.POST.get('password')
        contact = request.POST.get('contact')
        contact = '+92'+contact
        entry = User.objects.filter(email=email).count()
        if entry > 0 :
            data = {"error": "This email already exsists!"}
            return render(request, 'furugi/auth/register.html', data)
        
        entry = User.objects.filter(username=username).count()
        if entry > 0 :
            data = {"error": "This username already occupied"}
            return render(request, 'furugi/auth/register.html', data)
       
        if len(password) < 10:
            data = {"error": "Password is too short"}
            return render(request, 'furugi/auth/register.html', data)
            
        user = User.objects.create_user(username=username, email=email, password=password)
        user.save()
        login(request,user)
        Personal_Details.objects.create(user = user, contact = contact)
        return redirect('furugi:user-profile')
    else:
        data = {"error": None}
        return render(request, 'furugi/auth/register.html', data)

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
       
        user = authenticate(username=username, password=password)
        if user:           
            login(request,user)
            return redirect('/') 
        else:
            data = {"error": "Your username or password is incorrect!"}
            return render(request, 'furugi/auth/login.html', data)
    else:      
        data = {"error": None}
        return render(request, 'furugi/auth/login.html', data)

@login_required
def user_logout(request):
    logout(request)
    return redirect('furugi:index')

@login_required
def user_profile(request):
    user = request.user
    if request.method == 'POST':         
        data = Personal_Details.objects.filter(user = user) 
        contact = request.POST.get('contact')
        contact = '+92'+contact
        if request.POST.get('contact_visible') == 'on':
            visible = True          
        else:
            visible = False    

        try:
            details = Personal_Details.objects.update_or_create(user = user,
            defaults = {'contact': contact,'street' : request.POST.get('street'),
            'city' : request.POST.get('city'), 'country' : request.POST.get('country'),
            'gender' : request.POST.get('gender'), 
            'id_card_no' : request.POST.get('cnic'), 'contact_visible' : visible})
           

        except Personal_Details.DoesNotExist:
           raise Http500()
        return redirect('furugi:index')                  
    else:   
        try:  
            details = Personal_Details.objects.filter(user = user) 
            contact = details[0].contact
            contact = contact[3:] 
        except Personal_Details.DoesNotExist:
            raise Http404()
        if not details:
            data = {"details": ""}
        else:    
            data = {"details": details[0], "contact": contact}
        return render(request, 'furugi/account/edit.html', data)

@login_required
def view_profile(request):
    id = request.GET.get('id', None) 
    user = User.objects.get(id = id)       
    try:    
        data = Personal_Details.objects.filter(user = user)
    except Personal_Details.DoesNotExist:
        raise Http404()
    if not data:
            details = " "
    else:    
        details = data.values()
    username = str(user.username)
    email = str(user.email)
    result = {"details": list(details), "username": username, "email": email }
    return JsonResponse(result)

@login_required
def add_user_favorites(request, id):
    user = request.user                 
    dress = Dress.objects.get(id = id)
    exsist = Favorites.objects.filter( dress = dress).count()
    if exsist > 0 :
        messages.add_message(request, messages.INFO, 'The '+ dress.title +' is already in your favorites.')
    else:    
        try:
            favorites = Favorites.objects.create(user = user, dress = dress)    
        except Favorites.DoesNotExist:
            raise Http500()
        messages.add_message(request, messages.SUCCESS, 'The '+ dress.title +' has been added to your favorites.')
    return redirect('/ads')  

@login_required
def add_to_cart(request):
    user = request.user   
    id = request.GET.get('id', None)              
    dress = Dress.objects.get(id = id)
    exsist = Cart.objects.filter( dress = dress).count()
    if exsist > 0 :
        text = 'Warning: The '+ dress.title +' is already in your bag'
        items = 0
    else:    
        try:
            cart = Cart.objects.create(user = user, dress = dress)    
        except Cart.DoesNotExist:
            raise Http500()
        text = 'Success: The '+ dress.title +' has been added to your bag'
        items = Cart.objects.filter(user = user).count()
    data = { "text" : text, "items" : items}
    return JsonResponse(data) 

@login_required
def remove_user_favorites(request, id):     
    dress = Dress.objects.get(id = id)
    try:
        favorites = Favorites.objects.get(dress = dress).delete()  
    except Favorites.DoesNotExist:
        raise Http404()  
    messages.add_message(request, messages.ERROR, 'The '+ dress.title +' has been removed from favorites.')
    return redirect('/user-favorites')           

@login_required
def user_favorites(request):    
    user = request.user
    try:
        data = Favorites.objects.filter(user = user).select_related('dress')
    except Favorites.DoesNotExist:
        raise Http404()
    tag = Dress.tags.most_common()[:20]
    trending = Trending.objects.all()
    start = randrange(15,25)        
    rated = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-posted_date')[start:][:11]
      
    data = {"dresses": data, "tags": tag,  'trending': trending, 'rated': rated}
    return render(request, 'furugi/account/favorites.html', data)

@login_required
def user_cart(request):    
    user = request.user
    try:
        data = Cart.objects.filter(user = user).select_related('dress')
    except Cart.DoesNotExist:
        raise Http404()
    details = Personal_Details.objects.get(user = user)
    data = {"dresses": data, "details": details}
    return render(request, 'furugi/account/cart.html', data)

@login_required
def user_ads(request):
    user = request.user
    try:
        dress = Dress.objects.filter(user = user).order_by('-posted_date') 
    except Dress.DoesNotExist:
        raise Http404()

    page = request.GET.get('page', 1)
    paginator = Paginator(dress, 16)
    try:
        dress = paginator.page(page)
    except PageNotAnInteger:
        dress = paginator.page(1)
    except EmptyPage:
        dress = paginator.page(paginator.num_pages)
    tag = Dress.tags.most_common()[:20]
    trending = Trending.objects.all()
    start = randrange(15,25)        
    rated = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-posted_date')[start:][:11]
       
    data = {"dresses": dress, "tags": tag,  'trending': trending, 'rated': rated}
    return render(request, 'furugi/account/ads.html', data)

@login_required
def report_user(request, u_id, d_id):
    repoter = request.user
    user = User.objects.get(id = u_id)
    try:
        Report.objects.create(reporter = repoter, user = user, report = request.POST.get('report'))
    except Report.DoesNotExist:
        raise Http500()
    id = str(d_id)
    dress = Dress.objects.get(id = id)
    messages.add_message(request, messages.INFO, 'This user has been reported.')  
    return redirect('/ad-view/'+ id+'/'+dress.slug)

@login_required
def block_user(request, id):
    repoter = request.user
    user = User.objects.get(id = id)
    try:
        Block.objects.create(reporter = repoter, user = user)
    except Block.DoesNotExist:
        raise Http500() 
    return redirect('/chatbox')