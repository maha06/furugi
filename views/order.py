from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from ..models import Dress, Personal_Details, Cart, Order
from datetime import datetime, date
from django.templatetags.static import static
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from taggit.models import Tag
from django.core.mail import send_mail
from django.http import Http404
from django.contrib.auth.decorators import login_required
from twilio.rest import Client
from django.template.loader import render_to_string
from django.utils.html import strip_tags


@login_required
def delete_item(request, id):  
    dress = Dress.objects.get(id =id)  
    user = request.user  
    try:
        Cart.objects.get(user = user , dress = dress).delete()
    except Cart.DoesNotExist:
        raise Http404()
    messages.add_message(request, messages.ERROR, 'Item Removed')    
    return redirect('/user-cart')

@login_required
def create_order(request): 
    user = request.user 
    contact = request.POST.get("contact")
    address = request.POST.get("address")
    
    cart = Cart.objects.filter(user = user)  

    for c in cart: 
        try:
            Order.objects.create(dress = c.dress , contact = contact, street = address, responsible = c.dress.user, reciever = user )
            dress = Dress.objects.get(id = c.dress.id)
            dress.quantity = dress.quantity - 1
            dress.save()
            try:
                profile = Personal_Details.objects.get(user = c.dress.user)
                if not profile:
                    number = ''
                else:
                    number = str(profile.contact)
                    sendMsg(request, number)    
            except Exception as e:
                pass              
            email = c.dress.user.email    
            order_notification(request, email)
        except Order.DoesNotExist:
            raise Http404()
    Cart.objects.filter(user = user).delete()
  
    messages.add_message(request, messages.SUCCESS, 'Order successfully placed ! It will be delivered within a week. Keep a check on your email and mobile. Go and Shop more.')    
    return redirect('/past-orders')

@login_required
def user_orders(request):    
    user = request.user
    try:
        data = Order.objects.filter(responsible = user, delivered = 0).select_related('dress').order_by('-date')
    except Order.DoesNotExist:
        raise Http404()
    
    data = {"orders": data}
    return render(request, 'furugi/orders.html', data)

@login_required
def deliver_order(request, id):  
    order = Order.objects.get(id =id)  
    order.delivered = 1
    order.date_delivered = datetime.now()
    order.save()
    return redirect('/user-orders')

@login_required
def cancel_order(request, id):  
    order = Order.objects.get(id =id)  
    order.cancelled = 1
    order.save()
    return redirect('/past-orders')

@login_required
def dispatch_order(request, id):  
    order = Order.objects.get(id =id)  
    order.dispatched = 1
    order.save()
    return redirect('/user-orders')

@login_required
def delete_order(request, id):  
    order = Order.objects.get(id =id).delete()  
    return redirect('/user-orders')

@login_required
def get_items(request):
    user = request.user
    items = Cart.objects.filter(user = user).count()
    data = { "items" : items}
    return JsonResponse(data)

@login_required
def past_orders(request):    
    user = request.user
    try:
        data1 = Order.objects.filter(reciever = user, delivered = 0, cancelled = 0).select_related('dress').select_related('responsible').order_by('-date')
        data2 = Order.objects.filter(reciever = user, delivered = 1, cancelled = 0).select_related('dress').select_related('responsible').order_by('-date')
    except Order.DoesNotExist:
        raise Http404()    
    data = {"ordersND": data1, "ordersD": data2}
    return render(request, 'furugi/account/past_orders.html', data)

@login_required
def sendMsg( request, number):
    account_sid = "AC733c99c53e17dc55c875fec65b77888c"
    auth_token  = "e28847557e7008dab4a2f8b7702f8138"
    client = Client(account_sid, auth_token)

    if number.startswith('0'):
        x = number
        remove = x[1:]
        number = '+92'+remove
    try:    
        message = client.messages.create(
        body="Dear costumer,\n You have recieved new order at FURUGI. Visit 'Orders' section on website now and make sure to fulfill that order within two days as delaying in delivery might restrict your access from FURUGI.\n Regards, FURUGI Team",
        to= number,
        from_="+16105467688")
    except Exception as e:
       pass
    
@login_required
def orders_history(request):    
    user = request.user
    try:
        data = Order.objects.filter(responsible = user, delivered = 1).select_related('dress').order_by('-date')
    
    except Order.DoesNotExist:
        raise Http404()
    
    data = {"orders": data}
    return render(request, 'furugi/orders_history.html', data)

@login_required
def get_orders(request):    
    user = request.user
    try:
        data = Order.objects.filter(responsible = user, delivered = 0).count()
    
    except Order.DoesNotExist:
        raise Http404()
    
    data = {"orders": data}
    return JsonResponse(data)

@login_required
def order_notification(request , email):
    print('email-recived = '+ email)
    subject = "New Order Notification"
    html_message = render_to_string('furugi/emails/order_notification.html', {'context': 'values'})
    plain_message = strip_tags(html_message)
    send_mail(subject, plain_message, 'ORDER NOTIFICATION <contact@furugi.com.pk>',[email], html_message=html_message)
