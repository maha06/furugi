from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from ..models import Dress, Dress_Pictures, Trending, Personal_Details, Favorites, Dress_Rating, Pictures, Thread, Chat, User_Thread, Report, Ad_Thread
from datetime import datetime, date
from django.templatetags.static import static
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from taggit.models import Tag
from django.core.mail import send_mail
from django.db.models import Avg
from django.http import Http404
from django.contrib.postgres.search import TrigramSimilarity
from django.db.models.functions import Greatest
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from random import randrange

@login_required
def add_advertisment(request):
   
    if request.method == 'POST':       
        user = request.user
        tag_list = []
        files = request.FILES.getlist('pro-image')
        date = request.POST.get('date', False)
        tagsInput = request.POST["tags"]
        tags = tagsInput.strip()
        if tags.startswith(','):
            tags = tags[1:]
        tag_list = iter(tags.split(","))

        if date == '':
            date = None

        profile = files[0]
        try:
            dress = Dress.objects.create(user = user, title = request.POST.get('title'), description = request.POST.get('description'),
            category = request.POST.get('category'), 
            gender = request.POST.get('gender'), quantity = request.POST.get('quantity'), 
            price = request.POST.get('price'), profile = profile)  
        except Dress.DoesNotExist:
            raise Http500()

        for tag in tag_list:   
            dress.tags.add(tag)
    
        dress = Dress.objects.latest('id')  
        for photo in files[1:]:  
            print(photo)            
            Dress_Pictures.objects.create(dress =  dress, file = photo)
        return redirect('/ads')
    else:
        if(request.user.is_active):    
            tags = Dress.tags.most_common()[:8]
            #tags = ["cotton", "embroided", "silk", "chiffon", "khaddar", "mariab", "nishat", "linen", "organza"]
            data = {'tags' : tags}       
            return render(request, 'furugi/advertisment/create.html', data)
        else:            
            return redirect('/login')
            
def add_advertisment_pictures(dress, photo):  
    Dress_Pictures.objects.create(dress =  dress, file = photo) 


def ads(request):
    request.session['category'] = 'None'
    request.session['price'] = 10000
    request.session['gender'] = 'Not Specified'
    try:
        dress = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-posted_date')   
    except Dress.DoesNotExist:
        raise Http404()
    page = request.GET.get('page', 1)
    paginator = Paginator(dress, 19)
    try:
        dress = paginator.page(page)
    except PageNotAnInteger:
        dress = paginator.page(1)
    except EmptyPage:
        dress = paginator.page(paginator.num_pages)

    tag = Dress.tags.most_common()[:20]
    trending = Trending.objects.all()
    start = randrange(15,25)        
    rated = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-posted_date')[start:][:11]
      
    data = {"dresses": dress, "tags": tag, 'trending': trending, 'rated': rated }
    return render(request, 'furugi/listings.html', data)

def filtered_ads(request, slug):
    try:    
        dress = Dress.objects.filter(tags__name__in=[slug]).order_by('-posted_date')
    except Dress.DoesNotExist:
        raise Http404()
    page = request.GET.get('page', 1)

    paginator = Paginator(dress, 19)
    try:
        dress = paginator.page(page)
    except PageNotAnInteger:
        dress = paginator.page(1)
    except EmptyPage:
        dress = paginator.page(paginator.num_pages)
     
    tag = Dress.tags.most_common()[:20]
    trending = Trending.objects.all()
    start = randrange(15,25)        
    rated = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-posted_date')[start:][:11]
      
    data = {"dresses": dress, "tags": tag, 'trending': trending, 'rated': rated}

    return render(request, 'furugi/listings.html', data)

def view_advertisment(request, id, slug):      
    try:
        dressGrp = Dress.objects.filter(id = id).select_related('user') 
    except Dress.DoesNotExist:
        raise Http404("Dress does not exist")
    pics = Dress_Pictures.objects.filter(dress = id)
    reviews = Dress_Rating.objects.filter(dress = id)
    try:
        dress = Dress.objects.get(id = id)
    except Dress.DoesNotExist:
        raise Http404("Dress does not exist")
    tags = dress.tags.all()
    user = dressGrp[0].user
    try:       
        details = Personal_Details.objects.get(user = dressGrp[0].user)
    except:
        details = ''    
    dress.views = dress.views + 1    
    dress.save()
    trending = Trending.objects.all()
    start = randrange(15,25)        
    rated = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-posted_date')[start:][:11]
    ads = Dress.objects.filter(user = user).order_by('-posted_date')  
    data = {"dress": dressGrp[0], "ads":ads, "pics": pics, "tags":tags, "reviews": reviews, "details": details, "trending":trending, "rated":rated}
    return render(request, 'furugi/advertisment/view.html', data)

@login_required
def edit_advertisment(request, id):
    if request.method == 'POST':
        files = request.FILES.getlist('pro-image', False)     
        available_on = request.POST.get('date')
        tags = request.POST["tags"]        
        dress = Dress.objects.get(id = id)
          
        if available_on == '':
            available_on = dress.available_on
                         
        if files == False:
            profile = dress.profile.name
        else:
            profile = files[0]
    
        try:
            dress = Dress.objects.get(id = id)
            dress.title = request.POST.get('title')
            dress.description = request.POST.get('description')
            dress.category = request.POST.get('category') 
            dress.gender = request.POST.get('gender')
            dress.quantity = request.POST.get('quantity')
            dress.price = request.POST.get('price')
            dress.profile = profile
            dress.save()
        except Dress.DoesNotExist:
            raise Http500()
        if files != False:
            Dress_Pictures.objects.filter(dress = Dress.objects.get(id = id)).delete()           
            for photo in files[1:]:        
                Dress_Pictures.objects.create(dress = Dress.objects.get(id = id) , file = photo)                 
        
        if tags != '':
            dress.tags.clear()
            check = (tags.replace(","," ")) 
            tag_list = iter(check.split(" "))
            for tag in tag_list:   
                dress.tags.add(tag)
            
        messages.add_message(request, messages.SUCCESS, 'Your ad has been updated successfully.')    
        return redirect('/user-ads')
    else:
        dress = Dress.objects.get(id = id)
        pics = Dress_Pictures.objects.filter(dress = id).select_related('dress')
        data = {"dress": dress, "pics": pics}
        return render(request, 'furugi/advertisment/edit.html', data)

@login_required
def delete_advertisment(request, id):      
    try:
        Dress.objects.get(id = id).delete()
    except Dress.DoesNotExist:
        raise Http404()
    messages.add_message(request, messages.ERROR, 'Your ad has been deleted successfully.')    
    return redirect('/user-ads')

@login_required
def add_review(request): 
    name = request.user.username
    review = request.GET.get('review', None)
    stars = request.GET.get('stars', None)
    id = request.GET.get('id', None)
    try:
        save = Dress_Rating.objects.create(dress = Dress.objects.get(id = id),
        stars = stars, review = review, user_name = name )
    except Dress_Rating.DoesNotExist:
        raise Http500()
    try:
        reviews = Dress_Rating.objects.filter(dress = Dress.objects.get(id = id)).values()
    except Dress_Rating.DoesNotExist:
        raise Http404()
    data = { "reviews" : list(reviews)}
    return JsonResponse(data)

def search_results(request):
    gender = request.POST.get('gender')
    title = request.POST.get('title')
    price = request.POST.get('price')
    category = request.POST.get('category')
    request.session['category'] = category
    request.session['price'] = price
    request.session['gender'] = gender
    if title == None:
        if category == 'None' and gender == 'Not Specified':            
            try:
                print('here')
                data = Dress.objects.filter(price__lte = int(price)).order_by('-posted_date')[:22]  
              
            except Dress.DoesNotExist:
                raise Http404()

        elif category == 'None':            
            try:
                print('there')
                #data = Dress.objects.filter(gender = gender, price__lte = int(price)).order_by('-posted_date')[:22]   
                data = Dress.objects.filter(gender = gender, price__lte = int(price)).order_by('-posted_date')[:22]   
                
            except Dress.DoesNotExist:
                raise Http404()

        elif gender == 'Not Specified':            
            try:
                print('mere')
                data = Dress.objects.filter(category = category, price__lte = int(price)).order_by('-posted_date')[:22]   
            
            except Dress.DoesNotExist:
                raise Http404()
        else:           
            try:
                print('chere')
                data = Dress.objects.filter(gender = gender, category = category, price__lte = int(price)).order_by('-posted_date')[:22]   
                print(data)
            except Dress.DoesNotExist:
                raise Http404()
    else:
        try:
            data = Dress.objects.annotate(
            similarity = Greatest(
            TrigramSimilarity('description', title),
            TrigramSimilarity('title', title),
            TrigramSimilarity('category', title)
            )).filter(
            similarity__gt=0.07).order_by('-similarity').order_by('-posted_date')
        except Dress.DoesNotExist:
            raise Http404()

    tag = Dress.tags.most_common()[:20]
    trending = Trending.objects.all()
    start = randrange(15,25)        
    rated = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-posted_date')[start:][:11]

    data = {"dresses": data, "tags": tag, 'trending': trending, "rated":rated}
    return render(request, 'furugi/listings.html', data)

def category_results(request, category):
    request.session['category'] = category
    request.session['price'] = 10000
    request.session['gender'] = 'Not Specified'
    try:
        data = Dress.objects.filter(category = category).order_by('-posted_date')   
    except Dress.DoesNotExist:
        raise Http404()

    tag = Dress.tags.most_common()[:20]
    trending = Trending.objects.all()
    start = randrange(15,25)        
    rated = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-posted_date')[start:][:11]
      
    page = request.GET.get('page', 1)
    paginator = Paginator(data, 19)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    data = {"dresses": data, "tags": tag, 'trending': trending, "rated":rated}
    return render(request, 'furugi/listings.html', data)

def sort_ads(request):
    category = request.session['category']
    price = request.session['price']
    gender = request.session['gender']
    print(category)
    print(price)
    print(gender)
    sort = request.POST.get('sort')
    if sort == 'Newest':
        if category == 'None' and gender == 'Not Specified':            
            try:
                data = Dress.objects.filter(price__lte = int(price)).order_by('-posted_date')[:22]  
                print('here')
            except Dress.DoesNotExist:
                raise Http404()

        elif category == 'None':      
            print('there')      
            try:
                data = Dress.objects.filter(gender = gender, price__lte = int(price)).order_by('-posted_date')[:22]   
                   
            except Dress.DoesNotExist:
                raise Http404()

        elif gender == 'Not Specified':            
            try:
                data = Dress.objects.filter(category = category, price__lte = int(price)).order_by('-posted_date')[:22]   
                print('mere')
            except Dress.DoesNotExist:
                raise Http404()
        else:           
            try:
                data = Dress.objects.filter(gender = gender, category = category, price__lte = int(price)).order_by('-posted_date')[:22]   
                print('chere')
            except Dress.DoesNotExist:
                raise Http404()
    
    elif sort == 'Oldest':
        if category == 'None' and gender == 'Not Specified':            
            try:
                data = Dress.objects.filter(price__lte = int(price)).order_by('posted_date')[:22]  
                print(data) 
            except Dress.DoesNotExist:
                raise Http404()

        elif category == 'None':            
            try:
                data = Dress.objects.filter(gender = gender, price__lte = int(price)).order_by('posted_date')[:22]   
                   
            except Dress.DoesNotExist:
                raise Http404()

        elif gender == 'Not Specified':            
            try:
                data = Dress.objects.filter(category = category, price__lte = int(price)).order_by('posted_date')[:22]   
                print(data)
            except Dress.DoesNotExist:
                raise Http404()
        else:           
            try:
                data = Dress.objects.filter(gender = gender, category = category, price__lte = int(price)).order_by('posted_date')[:22]   
             
            except Dress.DoesNotExist:
                raise Http404()
    

    tag = Dress.tags.most_common()[:20]
    trending = Trending.objects.all()
    start = randrange(15,25)        
    rated = Dress.objects.annotate(stars = Avg('dress_rating__stars')).order_by('-posted_date')[start:][:11]
      
    data = {"dresses": data, "tags": tag, 'trending': trending, 'rated': rated }
    return render(request, 'furugi/listings.html', data)

  