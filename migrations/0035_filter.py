# Generated by Django 3.0.4 on 2020-10-12 18:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('furugi', '0034_auto_20200923_1957'),
    ]

    operations = [
        migrations.CreateModel(
            name='Filter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.CharField(max_length=200)),
                ('gender', models.CharField(default='Female', max_length=200)),
                ('price', models.IntegerField(default=0)),
            ],
        ),
    ]
