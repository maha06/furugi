from django.db import models
from datetime import datetime  
from taggit.managers import TaggableManager
from django.contrib.auth.models import User 
from ckeditor.fields import RichTextField
from django.template.defaultfilters import slugify

# Create your models here.
class Personal_Details(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null= True)
    contact = models.CharField(max_length=13, null=True, default='0000000')
    street = models.CharField(max_length=1000, null=True)
    city = models.CharField(max_length=200, null=True)
    country = models.CharField(max_length=200, null=True)
    gender = models.CharField(max_length=200, null=True)
    id_card_no = models.CharField( null=True, max_length=13)
    contact_visible = models.BooleanField(default= 0)
   
class Dress(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=500)
    description = models.CharField(max_length=10000, null=True)
    category = models.CharField(max_length=200, null=False)
    gender = models.CharField(max_length=200, null=False, default="Female")
    price = models.IntegerField(default=0, null=False)
    views = models.IntegerField(default=0)
    quantity = models.IntegerField(default=1)
    posted_date = models.DateTimeField(default=datetime.now, blank=True)
    profile= models.FileField(upload_to='images/', null=False, verbose_name="", default="null")
    tags = TaggableManager()
    slug = models.SlugField(blank = True, null = True, max_length=500)
    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Dress, self).save(*args, **kwargs)

class Dress_Rating(models.Model):
    dress = models.ForeignKey(Dress, on_delete=models.CASCADE)
    user_name = models.CharField(max_length=200 , default="user")
    stars = models.IntegerField(default=5)    
    rating_date = models.DateTimeField(default=datetime.now)
    review = models.CharField(max_length=10000)

class Pictures(models.Model):
    pic= models.FileField(upload_to='images/', null=False, verbose_name="", default="null")

class Dress_Pictures(models.Model):
    dress = models.ForeignKey(Dress, on_delete=models.CASCADE)
    file = models.FileField(upload_to='images/', null=True, verbose_name="", default="null") 

class Favorites(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    dress = models.ForeignKey(Dress, on_delete=models.CASCADE)  

class Thread(models.Model):
    timestamp = models.DateTimeField(default=datetime.now)

class Chat(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.CharField(max_length=10000)
    thread =  models.ForeignKey(Thread, on_delete=models.CASCADE)
    read = models.BooleanField(default= 0)
    timestamp = models.DateTimeField(default=datetime.now)

class User_Thread(models.Model):
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE) 
    sender = models.ForeignKey(User, related_name='sender', on_delete=models.CASCADE)
    reciever = models.ForeignKey(User, related_name='reciever', on_delete=models.CASCADE)
    
class Ad_Thread(models.Model):
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE) 
    dress = models.ForeignKey(Dress, on_delete=models.CASCADE)
       
class Report(models.Model):
    reporter = models.ForeignKey(User, related_name='reporter', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='user',on_delete=models.CASCADE)
    report = models.CharField(max_length=2000)
    timestamp = models.DateTimeField(default=datetime.now)

class Block(models.Model):
    reporter = models.ForeignKey(User, related_name='blocker', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='blocked',on_delete=models.CASCADE)
    timestamp = models.DateTimeField(default=datetime.now)

class Blog(models.Model):
    title = models.CharField(max_length=1000)
    date = models.DateTimeField(default=datetime.now)
    profile= models.FileField(upload_to='images/', null=False, verbose_name="", default="null")
    body = RichTextField(blank=True, max_length=10000)
    slug = models.SlugField(blank = True, null = True, max_length=1000)
    tags = TaggableManager()
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Blog, self).save(*args, **kwargs)

class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    dress = models.ForeignKey(Dress, on_delete=models.CASCADE)  

class Order(models.Model):
    responsible = models.ForeignKey(User, related_name='responsible', on_delete=models.CASCADE, null=True)
    reciever = models.ForeignKey(User, related_name='r', on_delete=models.CASCADE, null=True)
    dress = models.ForeignKey(Dress, on_delete=models.CASCADE, null=True) 
    date = models.DateTimeField(default=datetime.now)
    contact = models.CharField(max_length=13, null=True, default='0000000')
    street = models.CharField(max_length=1000, null=True)
    dispatched = models.BooleanField(default= 0)
    cancelled = models.BooleanField(default= 0)
    delivered = models.BooleanField(default= 0)
    date_delivered = models.DateTimeField(null= True)

class Trending(models.Model):
    dress = models.ForeignKey(Dress, on_delete=models.CASCADE)



   

