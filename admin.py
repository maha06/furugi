from django.contrib import admin
from .models import Dress, Dress_Pictures, Trending, Order, Cart, Personal_Details, Dress_Rating, Favorites, Thread, Chat, User_Thread, Report, Blog, Block

# Register your models here.

admin.site.register(Dress)
admin.site.register(Dress_Pictures)
admin.site.register(Personal_Details)
admin.site.register(Dress_Rating)
admin.site.register(Favorites)
admin.site.register(Thread)
admin.site.register(Chat)
admin.site.register(User_Thread)
admin.site.register(Report)
admin.site.register(Blog)
admin.site.register(Cart)
admin.site.register(Order)
admin.site.register(Trending)
admin.site.register(Block)